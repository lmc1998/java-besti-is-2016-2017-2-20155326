package ch03;
public class AmstrongNum {
    public static void main(String[] args) {
        int i, j, k;
        System.out.print("All of the Amstrong Numbers : ");
        for(int num = 100; num < 1000; num++) {
            i = num / 100;
            j = (num / 10) % 10;
            k = num % 10;
            i = i * i * i;
            j = j * j * j;
            k = k * k * k;
            if(i + j + k == num) {
                System.out.print(num+" ");
            }
        }
        System.out.println();
    }
}